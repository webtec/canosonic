	    <footer class="footer">
	      <div class="container">
	      	<div class="row">
	      		<div class="col-md-4 col-xs-12">
	      			<?php printf(esc_html__('Todos los derechos reservados %1$s %2$s %3$s', 'onepress'), '&copy;', esc_attr(date('Y')), esc_attr(get_bloginfo())); ?>
	      		</div>
	      		<div class="col-md-8 col-xs-12">
	      			<?php get_sidebar('footer'); ?>
	      		</div>
	      	</div>
	      </div>
	    </footer>
		<?php wp_footer(); ?>
</body>
</html>
