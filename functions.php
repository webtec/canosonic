<?php
require_once('wp_bootstrap_navwalker.php');	//Añade el archivo que carga el navbar

function register_my_menus() {	//Registra el navbar
   register_nav_menus(
      array(
         '<strong>primary</strong>'   => __( 'Primary Menu' )
      )
   );
}

if ( function_exists('register_sidebar') ) //Widget Footer Widgets Left
    register_sidebar(array(
        'name' => 'Footer Widgets Left',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));

if ( function_exists('register_sidebar') ) //Widget Footer Widgets Center
        register_sidebar(array(
            'name' => 'Footer Widgets Center',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2 class="widgettitle">',
            'after_title' => '</h2>',
        ));


if ( function_exists('register_sidebar') ) //Widget Footer Widgets Right
        register_sidebar(array(
            'name' => 'Footer Widgets Right',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2 class="widgettitle">',
            'after_title' => '</h2>',
        ));

add_action( 'init', 'register_my_menus' );	//Registra el menu del navbar (wp_bootstrap_navwalker.php)
add_filter( 'show_admin_bar', '__return_false' ); //Oculta la barra del administrador
add_theme_support( 'custom-header' );		//Permite editar el header
remove_filter ('the_content',  'wpautop');	//Remueve el filtro de los <p></p> automaticos
remove_filter ('comment_text', 'wpautop');	//Remueve el filtro de los <p></p> automaticos
add_filter( 'wp_title', 'get_title', 10,2 );


function get_title( $title, $sep )
{   
    global $paged, $page;
        
    if ( is_feed() )
        return $title;
        // Add the site name.
        $title .= get_bloginfo( 'name' );
        // Add the site description for the home/front page.
        $site_description = get_bloginfo( 'description' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            $title = "$title $sep $site_description";
        // Add a page number if necessary.
        if ( $paged >= 2 || $page >= 2 )
            $title = "$title $sep " . sprintf( __( 'Page', 'appointment' ), max( $paged, $page ) );
        return $title;
}  

function wpbootstrap_scripts()
{
	wp_enqueue_style('bs_css', get_template_directory_uri(). '/css/bootstrap.min.css');
	wp_enqueue_style('st_css', get_template_directory_uri(). '/css/sticky-footer-navbar.css');
	wp_enqueue_style('fa_css', get_template_directory_uri(). '/css/font-awesome.css');
    wp_enqueue_style('cf_css', get_template_directory_uri(). '/css/contactform.css');
    wp_enqueue_style('cs_css', get_template_directory_uri(). '/css/custom.css');
	
    wp_enqueue_script( 'jquery-script', get_template_directory_uri() . '/js/jquery.min.js', array(), '1.0.0', true);
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
	wp_enqueue_script( 'jquery-map', get_template_directory_uri() . '/js/jquery.imagemapster.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', array(), '1.0.0', true);
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts' );
?>